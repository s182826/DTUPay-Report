FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/transaction-0.0.1-SNAPSHOT.jar /usr/src
EXPOSE 8081
CMD java -jar transaction-0.0.1-SNAPSHOT.jar
