package dk.dtu.transaction.controller;

import dk.dtu.transaction.model.Transaction;
import dk.dtu.transaction.service.ReportGeneratorService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Set;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@RestController
@RequestMapping("/api/reports")
@Log4j2
public class ReportController {

    private final ReportGeneratorService reportGeneratorService;

    @Autowired
    public ReportController(ReportGeneratorService reportGeneratorService) {
        this.reportGeneratorService = reportGeneratorService;
    }

    @GetMapping(path = "/customer/{cprNumber}")
    Set<Transaction> getReportForCustomer(@PathVariable String cprNumber,
                                          @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                          @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate){
        log.info("Requesting reports for customer: " + cprNumber
                    + " in the interval: " + startDate + " - " + endDate);
        return reportGeneratorService.generateReportForCustomer(cprNumber, startDate, endDate);
    }

    @GetMapping(path = "/merchant/{cprNumber}")
    Set<Transaction> getReportForMerchant(@PathVariable String cprNumber,
                                           @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                           @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate){
        log.info("Requesting reports for merchant: " + cprNumber
                + " in the interval: " + startDate + " - " + endDate);
        return reportGeneratorService.generateReportForMerchant(cprNumber, startDate, endDate);
    }
}
