package dk.dtu.transaction.service;

import dk.dtu.transaction.model.Transaction;
import dk.dtu.transaction.repository.ITransactionRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Service
@Log4j2
public class TransactionService {

    private final ITransactionRepository transactionRepository;

    @Autowired
    public TransactionService(ITransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Transaction getTransactionById(UUID id) {
        log.info("Returning a transaction with: " + id);
        return transactionRepository.findById(id);
    }

    public void createTransaction(Transaction transaction) {
        log.info("Creating transaction: " + transaction);
        transactionRepository.createTransaction(transaction);
    }

    public void addCustomerRecord(String userCpr) {
        log.info("Adding user record: " + userCpr);
        transactionRepository.addCustomerRecord(userCpr);
    }

    public void addMerchantRecord(String userCpr) {
        log.info("Adding user record: " + userCpr);
        transactionRepository.addMerchantRecord(userCpr);
    }

    public Set<Transaction> getCustomerTransactionsByCpr(String cprNumber) {
        log.info("Getting transactions for: " + cprNumber);
        return transactionRepository.getCustomerTransactionsByCpr(cprNumber);
    }

    public Set<Transaction> getMerchantTransactionsByCpr(String cprNumber) {
        log.info("Getting transactions for: " + cprNumber);
        return transactionRepository.getMerchantTransactionsByCpr(cprNumber);
    }
}
