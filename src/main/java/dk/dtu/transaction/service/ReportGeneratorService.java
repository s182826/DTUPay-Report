package dk.dtu.transaction.service;

import dk.dtu.transaction.model.Transaction;
import dk.dtu.transaction.repository.ITransactionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Service
@AllArgsConstructor
@Log4j2
public class ReportGeneratorService {
    private final ITransactionRepository transactionRepository;

    public Set<Transaction> generateReportForCustomer(String customerCprNumber, LocalDate startDate, LocalDate endDate){
        log.info("Requesting report for customer: " + customerCprNumber + " in: " + startDate + " - " + endDate);
        Set<Transaction> transactions = new HashSet<>();

        for(Transaction transaction : transactionRepository.getCustomerTransactionsByCpr(customerCprNumber)){
            Transaction foundTransaction = transactionRepository.findById(transaction.getId());
            if(foundTransaction.dateInRange(startDate, endDate)){
                transactions.add(foundTransaction);
            }
        }
        return transactions;
    }

    public Set<Transaction> generateReportForMerchant(String merchantCprNumber, LocalDate startDate, LocalDate endDate){
        log.info("Requesting report for merchant: " + merchantCprNumber + " in: " + startDate + " - " + endDate);
        Set<Transaction> transactions = new HashSet<>();

        for(Transaction transaction : transactionRepository.getMerchantTransactionsByCpr(merchantCprNumber)){
            Transaction foundTransaction = transactionRepository.findById(transaction.getId());
            if(foundTransaction.dateInRange(startDate, endDate)){
                transaction.setCustomerCprNumber(null);
                transactions.add(foundTransaction);
            }
        }
        return transactions;
    }
}
