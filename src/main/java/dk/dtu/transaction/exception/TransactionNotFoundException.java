package dk.dtu.transaction.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TransactionNotFoundException extends RuntimeException {

    public TransactionNotFoundException(UUID id){
        super("Transaction not found with ID: " + id);
    }

}
