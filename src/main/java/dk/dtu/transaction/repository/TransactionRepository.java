package dk.dtu.transaction.repository;

import dk.dtu.transaction.model.Transaction;
import dk.dtu.transaction.exception.TransactionNotFoundException;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Component
public class TransactionRepository implements ITransactionRepository {

    private Set<Transaction> transactions = new HashSet<>();
    private Map<String, Set<Transaction>> customerTransactions = new HashMap<>();
    private Map<String, Set<Transaction>> merchantTransactions = new HashMap<>();

    @Override
    public void addCustomerRecord(String cprNumber) { customerTransactions.putIfAbsent(cprNumber, new HashSet<>()); }

    @Override
    public void addMerchantRecord(String cprNumber) { merchantTransactions.putIfAbsent(cprNumber, new HashSet<>()); }

    @Override
    public void createTransaction(Transaction transaction) {
        transactions.add(transaction);
        customerTransactions.get(transaction.getCustomerCprNumber()).add(transaction);
        merchantTransactions.get(transaction.getMerchantCprNumber()).add(transaction);
    }

    @Override
    public Transaction findById(UUID id) {
        Transaction foundTransaction = transactions.stream().filter(t -> t.getId().equals(id)).findFirst()
                .orElseThrow(() -> new TransactionNotFoundException(id));

        Transaction newTransaction = new Transaction();

        newTransaction.setId(foundTransaction.getId());
        newTransaction.setAmount(foundTransaction.getAmount());
        newTransaction.setCustomerCprNumber(foundTransaction.getCustomerCprNumber());
        newTransaction.setMerchantCprNumber(foundTransaction.getMerchantCprNumber());
        newTransaction.setDate(foundTransaction.getDate());

        return newTransaction;
    }

    @Override
    public Set<Transaction> getCustomerTransactionsByCpr(String cprNumber) {
        return customerTransactions.get(cprNumber);
    }

    @Override
    public Set<Transaction> getMerchantTransactionsByCpr(String cprNumber) {
        return merchantTransactions.get(cprNumber);
    }

}
