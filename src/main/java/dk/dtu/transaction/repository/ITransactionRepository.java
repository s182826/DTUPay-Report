package dk.dtu.transaction.repository;

import dk.dtu.transaction.model.Transaction;

import java.util.Set;
import java.util.UUID;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public interface ITransactionRepository {

    public void addCustomerRecord(String cprNumber);
    public void addMerchantRecord(String cprNumber);
    void createTransaction(Transaction transaction);
    Transaction findById(UUID id);
    Set<Transaction> getCustomerTransactionsByCpr(String cprNumber);
    Set<Transaction> getMerchantTransactionsByCpr(String cprNumber);

}
