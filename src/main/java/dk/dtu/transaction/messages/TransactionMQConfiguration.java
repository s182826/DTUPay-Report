package dk.dtu.transaction.messages;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Configuration
public class TransactionMQConfiguration {

    public static final String BANK_REFUND_EXCHANGE = "bank-transaction-exchange";
    public static final String BANK_REFUND_ROUTING_KEY = "bank-transaction-exchange-refund-routing-key";

    public static final String USER_EXCHANGE_NAME = "transaction-user-exchange";
    public static final String USER_ROUTING_KEY = "transaction-user-exchange-add-cpr-routing-key";
    public static final String QUEUE_USER_NAME = "userCreatedTransaction";

    public static final String REFUND_EXCHANGE_NAME = "refund-exchange";
    public static final String REFUND_ROUTING_KEY = "refund-exchange-routing-key";
    public static final String QUEUE_REFUND_NAME = "refundRequest";

    public static final String TRANSACTION_EXCHANGE_NAME = "transaction-exchange";
    public static final String TRANSACTION_ROUTING_KEY = "transaction-routing-key";
    public static final String TRANSACTION_QUEUE_NAME = "createTransaction";

    public static final String TRANSACTION_REFUND_EXCHANGE_NAME = "transaction-refund-exchange";
    public static final String TRANSACTION_REFUND_ROUTING_KEY = "transaction-refund-routing-key";
    public static final String TRANSACTION_REFUND_QUEUE_NAME = "createRefundTransaction";

    public static final String TRANSACTION_MERCHANT_EXCHANGE = "transaction-merchant-exchange";
    public static final String TRANSACTION_MERCHANT_ROUTING_KEY = "transaction-merchant-routing-key";
    public static final String TRANSACTION_MERCHANT_QUEUE = "merchantCreatedTransaction";

    @Bean
    public TopicExchange createRefundExchange() {
        return new TopicExchange(TRANSACTION_REFUND_EXCHANGE_NAME);
    }

    @Bean
    public Queue createRefundQueue() { return new Queue(TRANSACTION_REFUND_QUEUE_NAME); }

    @Bean
    public Binding declareBindingCreateRefund() {
        return BindingBuilder.bind(createRefundQueue()).to(createRefundExchange()).with(TRANSACTION_REFUND_ROUTING_KEY);
    }

    @Bean
    public TopicExchange merchantExchange() {
        return new TopicExchange(TRANSACTION_MERCHANT_EXCHANGE);
    }

    @Bean
    public Queue merchantQueue() { return new Queue(TRANSACTION_MERCHANT_QUEUE); }

    @Bean
    public Binding declareBindingMerchant() {
        return BindingBuilder.bind(merchantQueue()).to(merchantExchange()).with(TRANSACTION_MERCHANT_ROUTING_KEY);
    }

    @Bean
    public TopicExchange customerExchange() {
        return new TopicExchange(USER_EXCHANGE_NAME);
    }

    @Bean
    public Queue customerQueue() { return new Queue(QUEUE_USER_NAME); }

    @Bean
    public Binding declareBindingCustomer() {
        return BindingBuilder.bind(customerQueue()).to(customerExchange()).with(USER_ROUTING_KEY);
    }

    @Bean
    public TopicExchange refundExchange() {
        return new TopicExchange(REFUND_EXCHANGE_NAME);
    }

    @Bean
    public Queue refundQueue() { return new Queue(QUEUE_REFUND_NAME); }

    @Bean
    public Binding declareBindingRefund() {
        return BindingBuilder.bind(refundQueue()).to(refundExchange()).with(REFUND_ROUTING_KEY);
    }

    @Bean
    public TopicExchange transactionExchange() {
        return new TopicExchange(TRANSACTION_EXCHANGE_NAME);
    }

    @Bean
    public Queue transactionQueue() { return new Queue(TRANSACTION_QUEUE_NAME); }

    @Bean
    public Binding declareBindingTransaction() {
        return BindingBuilder.bind(transactionQueue()).to(transactionExchange()).with(TRANSACTION_ROUTING_KEY);
    }

}
