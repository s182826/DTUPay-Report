package dk.dtu.transaction.messages;

import com.google.gson.Gson;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Service
public class MessageSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public MessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendRefundTransfer(double amount, String debtorCprNumber, String creditorCprNumber, String description) {
        BankTransferMessage bankTransferMessage = new BankTransferMessage(amount, debtorCprNumber, creditorCprNumber, description);
        rabbitTemplate.convertAndSend(TransactionMQConfiguration.BANK_REFUND_EXCHANGE, TransactionMQConfiguration.BANK_REFUND_ROUTING_KEY,
                new Gson().toJson(bankTransferMessage));
    }

}
