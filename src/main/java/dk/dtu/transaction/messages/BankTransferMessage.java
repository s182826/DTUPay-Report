package dk.dtu.transaction.messages;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public final class BankTransferMessage implements Serializable {

    private final double amount;
    private final String debtorCprNumber;
    private final String creditorCprNumber;
    private final String description;

    public BankTransferMessage(@JsonProperty("amount") double amount,
                               @JsonProperty("debtorCprNumber") String debtorCprNumber,
                               @JsonProperty("creditorCprNumber") String creditorCprNumber,
                               @JsonProperty("description") String description) {
        this.amount = amount;
        this.debtorCprNumber = debtorCprNumber;
        this.creditorCprNumber = creditorCprNumber;
        this.description = description;
    }
}
