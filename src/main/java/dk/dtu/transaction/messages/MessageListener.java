package dk.dtu.transaction.messages;

import com.google.gson.*;
import dk.dtu.transaction.model.Transaction;
import dk.dtu.transaction.service.TransactionService;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.UUID;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Service
@Log4j2
public class MessageListener {

    private final TransactionService transactionService;
    private final MessageSender messageSender;
    private final Gson gson;

    @Autowired
    public MessageListener(TransactionService transactionService, MessageSender messageSender) {
        this.transactionService = transactionService;
        this.messageSender = messageSender;
        this.gson = new Gson();
    }

    @RabbitListener(queues = "userCreatedTransaction")
    public void userCreatedMessage(final Message message) {
        String body = new String(message.getBody());
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
        transactionService.addCustomerRecord(jsonObject.get("cprNumber").getAsString());
    }

    @RabbitListener(queues = "merchantCreatedTransaction")
    public void merchantCreatedMessage(final Message message) {
        String body = new String(message.getBody());
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
        transactionService.addMerchantRecord(jsonObject.get("cprNumber").getAsString());
    }

    @RabbitListener(queues = "refundRequest")
    public void refundRequestMessage(final Message message) {
        String body = new String(message.getBody());
        log.info("Received refund: " + body);
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
        UUID id = UUID.fromString(jsonObject.get("transactionUuid").getAsString());
        try{
            Transaction transaction = transactionService.getTransactionById(id);
            messageSender.sendRefundTransfer(transaction.getAmount(), transaction.getMerchantCprNumber(), transaction.getCustomerCprNumber(), jsonObject.get("description").getAsString());
        } catch(Exception e){
            throw new AmqpRejectAndDontRequeueException("Transaction not found with id: " + id);
        }
    }

    @RabbitListener(queues = "createTransaction")
    public void createTransactionMessage(final Message message) throws AmqpRejectAndDontRequeueException {
        String body = new String(message.getBody());
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
        log.info("Received transaction: " + jsonObject);
        JsonObject jsonDate = jsonObject.get("date").getAsJsonObject();
        LocalDate date = LocalDate.of(
                jsonDate.get("year").getAsInt(),
                jsonDate.get("month").getAsInt(),
                jsonDate.get("day").getAsInt()
        );
        Transaction transaction = new Transaction(jsonObject.get("amount").getAsDouble(),
                                                    jsonObject.get("debtorCprNumber").getAsString(),
                                                    jsonObject.get("creditorCprNumber").getAsString(),
                                                    jsonObject.get("description").getAsString(),
                                                    date);
        transactionService.createTransaction(transaction);
    }

    @RabbitListener(queues = "createRefundTransaction")
    public void createRefundTransactionMessage(final Message message) throws AmqpRejectAndDontRequeueException {
        String body = new String(message.getBody());
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
        log.info("Received refund transaction: " + jsonObject);
        JsonObject jsonDate = jsonObject.get("date").getAsJsonObject();
        LocalDate date = LocalDate.of(
                jsonDate.get("year").getAsInt(),
                jsonDate.get("month").getAsInt(),
                jsonDate.get("day").getAsInt()
        );
        Transaction transaction = new Transaction(jsonObject.get("amount").getAsDouble(),
                jsonObject.get("creditorCprNumber").getAsString(),
                jsonObject.get("debtorCprNumber").getAsString(),
                jsonObject.get("description").getAsString(),
                date);
        transactionService.createTransaction(transaction);
    }


}
