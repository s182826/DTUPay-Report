package dk.dtu.transaction.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Data
@NoArgsConstructor
public class Transaction {
    private UUID id;
    private double amount;
    private String customerCprNumber;
    private String merchantCprNumber;
    private String description;
    private LocalDate date;

    public Transaction(double amount, String customerCprNumber, String merchantCprNumber, String description, LocalDate date) {
        this.id = UUID.randomUUID();
        this.amount = amount;
        this.customerCprNumber = customerCprNumber;
        this.merchantCprNumber = merchantCprNumber;
        this.description = description;
        this.date = date;
    }

    public boolean dateInRange(LocalDate startDate, LocalDate endDate){
        return startDate.isBefore(this.date) && endDate.isAfter(this.date);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", customerCprNumber='" + customerCprNumber + '\'' +
                ", merchantCprNumber='" + merchantCprNumber + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
