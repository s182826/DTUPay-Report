package dk.dtu.transaction;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/dk/dtu/transaction/feature",
        monochrome = true
)
public class CucumberTest {
}
