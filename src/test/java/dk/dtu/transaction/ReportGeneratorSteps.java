package dk.dtu.transaction;

import dk.dtu.transaction.model.Transaction;
import dk.dtu.transaction.repository.ITransactionRepository;
import dk.dtu.transaction.repository.TransactionRepository;
import dk.dtu.transaction.service.ReportGeneratorService;
import dk.dtu.transaction.service.TransactionService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public class ReportGeneratorSteps {

    private ITransactionRepository transactionRepository;
    private ReportGeneratorService reportGeneratorService;
    private TransactionService transactionService;
    private Set<Transaction> transactionsInTheReport;
    private Set<Transaction> reportTransactionList;
    private String customerCpr = "1234";
    private String merchantCpr = "4321";
    private Transaction transaction;
    private LocalDate transactionDate;


    @Given("A customer with a transaction from {int} {int} {int}")
    public void aCustomer(int year, int month, int day) {
        transactionRepository = new TransactionRepository();
        transactionService = new TransactionService(transactionRepository);
        transactionsInTheReport = new HashSet<>();
        reportGeneratorService = new ReportGeneratorService(transactionRepository);
        transactionDate = LocalDate.of(year, month, day);
        transactionService.addMerchantRecord(merchantCpr);
        transactionService.addCustomerRecord(customerCpr);
        transaction = new Transaction(100, customerCpr, merchantCpr, "", transactionDate);
        transactionService.createTransaction(transaction);
    }

    @And("the customer adds a transaction from {int} {int} {int}")
    public void aTransactionFrom(int year, int month, int day) {
        transactionDate = LocalDate.of(year, month, day);
        transaction = new Transaction(200, customerCpr, merchantCpr, "", transactionDate);
        transactionService.createTransaction(transaction);
        transactionsInTheReport.add(transaction);
    }

    @When("The system sends reports to the customer for the interval {int} {int} {int} to {int} {int} {int}")
    public void theSystemSendsReportsToTheUserForTheIntervalTo(
            int yearStart, int monthStart, int dayStart,
            int yearEnd, int monthEnd, int dayEnd) {
        LocalDate startDate = LocalDate.of(yearStart, monthStart, dayStart);
        LocalDate endDate = LocalDate.of(yearEnd, monthEnd, dayEnd);
        reportTransactionList = reportGeneratorService
                .generateReportForCustomer(customerCpr, startDate, endDate);
    }

    @Then("The customer will receive the transactions inside the interval")
    public void theCustomerWillReceiveTheTransactionsInsideTheInterval() {
        assertEquals(transactionsInTheReport, reportTransactionList);
    }

    @Given("A merchant with a transaction from {int} {int} {int}")
    public void aMerchantWithATransactionFrom(int year, int month, int day) {
        transactionRepository = new TransactionRepository();
        transactionService = new TransactionService(transactionRepository);
        transactionsInTheReport = new HashSet<>();
        reportGeneratorService = new ReportGeneratorService(transactionRepository);
        transactionDate = LocalDate.of(year, month, day);
        transactionService.addMerchantRecord(merchantCpr);
        transactionService.addCustomerRecord(customerCpr);
        transaction = new Transaction(200, customerCpr, merchantCpr, "", transactionDate);
        transactionService.createTransaction(transaction);
    }

    @And("the merchant adds a transaction from {int} {int} {int}")
    public void theMerchantAddsATransactionFrom(int year, int month, int day) {
        transactionDate = LocalDate.of(year, month, day);
        transaction = new Transaction(100, customerCpr, merchantCpr, "", transactionDate);
        transactionService.createTransaction(transaction);
        transactionsInTheReport.add(transaction);
    }

    @When("The system sends reports to the merchant for the interval {int} {int} {int} to {int} {int} {int}")
    public void theSystemSendsReportsToTheMerchantForTheIntervalTo(
            int startYear, int startMonth, int startDay,
            int endYear, int endMonth, int endDay) {
        LocalDate startDate = LocalDate.of(startYear, startMonth, startDay);
        LocalDate endDate = LocalDate.of(endYear, endMonth, endDay);
        reportTransactionList = reportGeneratorService
                .generateReportForMerchant(merchantCpr, startDate, endDate);
    }

    @Then("The merchant will receive the transactions inside the interval")
    public void theMerchantWillReceiveTheTransactionsInsideTheInterval() {
        assertEquals(transactionsInTheReport, reportTransactionList);
    }
}

