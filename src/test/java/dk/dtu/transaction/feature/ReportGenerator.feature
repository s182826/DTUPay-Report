Feature: Report generator
  Description: Generates reports for the customer or the merchant

  Scenario: Customer gets a list of his transactions
    Given A customer with a transaction from 2019 12 15
    And the customer adds a transaction from 2020 1 5
    And the customer adds a transaction from 2020 1 10
    And the customer adds a transaction from 2020 1 18
    When The system sends reports to the customer for the interval 2020 1 1 to 2020 2 1
    Then The customer will receive the transactions inside the interval

  Scenario: Merchants gets a list of his transactions
    Given A merchant with a transaction from 2019 12 15
    And the merchant adds a transaction from 2020 1 5
    And the merchant adds a transaction from 2020 1 10
    And the merchant adds a transaction from 2020 1 18
    When The system sends reports to the merchant for the interval 2020 1 1 to 2020 2 1
    Then The merchant will receive the transactions inside the interval
