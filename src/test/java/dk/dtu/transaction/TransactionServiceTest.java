package dk.dtu.transaction;

import dk.dtu.transaction.exception.TransactionNotFoundException;
import dk.dtu.transaction.model.Transaction;
import dk.dtu.transaction.repository.ITransactionRepository;
import dk.dtu.transaction.repository.TransactionRepository;
import dk.dtu.transaction.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public class TransactionServiceTest {

    private TransactionService transactionService;
    private final String cprNumber = "1234";
    private final String merchantCpr = "4321";
    private String errorMessage;

    @BeforeEach
    public void init() {
        ITransactionRepository transactionRepository = new TransactionRepository();
        transactionService = new TransactionService(transactionRepository);
    }

    @Test
    public void addUserRecord() {
        transactionService.addCustomerRecord(cprNumber);
        Set<Transaction> setCustomer = transactionService.getCustomerTransactionsByCpr(cprNumber);
        assertTrue(setCustomer.isEmpty());
    }

    @Test
    public void getPaymentTransaction() {
        transactionService.addMerchantRecord(merchantCpr);
        transactionService.addCustomerRecord(cprNumber);
        Transaction transaction = new Transaction(0.0, cprNumber, merchantCpr, "", LocalDate.now());
        transactionService.createTransaction(transaction);
        assertTrue(transactionService.getCustomerTransactionsByCpr(cprNumber).contains(transaction));
        assertTrue(transactionService.getMerchantTransactionsByCpr(merchantCpr).contains(transaction));
    }

    @Test
    public void getTransaction() {
        UUID id = UUID.randomUUID();
        Transaction transaction = new Transaction(0.0, cprNumber, merchantCpr, "", LocalDate.now());
        transaction.setId(id);
        transactionService.addMerchantRecord(merchantCpr);
        transactionService.addCustomerRecord(cprNumber);
        transactionService.createTransaction(transaction);
        assertEquals(transaction, transactionService.getTransactionById(id));
    }

    @Test
    public void getTransactionThatDoesNotExist() {
        UUID id = UUID.randomUUID();
        try {
            transactionService.getTransactionById(id);
        }catch (TransactionNotFoundException e){
            errorMessage = e.getMessage();
        }
        assertEquals("Transaction not found with ID: " + id, errorMessage);
    }

}
