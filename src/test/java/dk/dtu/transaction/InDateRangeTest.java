package dk.dtu.transaction;


import dk.dtu.transaction.model.Transaction;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import static org.junit.Assert.*;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public class InDateRangeTest {

    @Test
    public void testInDateRangeTrue() {
        Transaction transaction = new Transaction(0.0, null, null, "", LocalDate.now());
        LocalDate beforeDate = LocalDate.of(2019,1,1);
        LocalDate endDate = LocalDate.of(2021,1,1);
        assertTrue(transaction.dateInRange(beforeDate, endDate));
    }

    @Test
    public void testInDateRangeFalse() {
        Transaction transaction = new Transaction(0.0, null, null, "", LocalDate.now());
        LocalDate beforeDate = LocalDate.of(2019,1,1);
        LocalDate endDate = LocalDate.of(2020,1,1);
        assertFalse(transaction.dateInRange(beforeDate, endDate));
    }

}
